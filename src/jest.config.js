// jest.config.js
module.exports = {
    preset: "jest-preset-angular",
    roots: ['src'],
    setupTestFrameworkScriptFile: "<rootDir>/src/setupJest.ts",
    verbose: true
};
