import { Component } from '@angular/core';

@Component({
  selector: 'ngw-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ngw';
}
