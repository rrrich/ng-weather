# Create the image based on the official Node 8.11 image from Dockerhub
FROM node:8.11 as node

ARG ENVIRONMENT

WORKDIR /
RUN pwd
COPY package.json /
COPY setupJest.ts /


# Install dependencies using npm
RUN npm install

#Build the app
RUN npm run test

WORKDIR /app

# Get all the code needed to run the app
COPY ./ /app/



RUN npm run build